# GIT Setup
In case you need to have two git configs for several projects inside different directories instead of per project...

Edit your ~/.gitconfig file and add the following two lines at the bottom:


    [includeIf "gitdir:/"]
        path = ~/.gitconfig-work
    [includeIf "gitdir:~/projects/personal/"]
        path = ~/.gitconfig-personal

***NOTE: REMEMBER ADDING A TRAILING SLASH ON THE gitdir: ***

Now add the following to both these files (~/.gitconfig-work and ~/.gitconfig-personal):

    [user]
        name = <your name>
        email = <your e-mail address>
  
The next steps is to remove the [user] block from the main .gitconfig

Also it will be very useful to add the following settings to one or both files. This will keep the git history of projects clean:

    [branch]
        autoSetupRebase = always

    [rebase]
        autoStash = true

    [push]
        default = simple
        followTags = true
            
    [merge]
        ff = only

You can check if you have the correct config in a certain directory by running git config -l

### Helpful aliases

    [alias]
        branch-sort = for-each-ref --sort=-committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
        lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ci) %C(bold blue)<%an>%Creset'
        lgdshort = log --pretty=oneline --stat --graph --decorate
        lgd = log -p --pretty=oneline
