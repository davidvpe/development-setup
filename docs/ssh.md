# SSH Setup

The key should have a good passphrase set (minimal 12 characters)

It should at least be a 4096 bit RSA key but we strongly encourage the usage of Ed25519 keys

## Generating a key
To generate a proper up-to-date key use the following command

`ssh-keygen -a 100 -t ed25519 -C "<my e-mail address>"`

Add the following content in your ~/.ssh/config file

    Host *
    AddKeysToAgent yes
    UseKeychain yes
    IdentityFile ~/.ssh/id_ed25519
    IdentityFile ~/.ssh/id_rsa 
