# My Setup
This are a set of instructions for setting up my own environment

### SSH Configuration
For the SSH Setup go [here](./docs/ssh.md)

### Git Setup
For the GIT Setup go [here](./docs/git.md)

### LSD
For beutifying the LS command output download [here](https://github.com/Peltoche/lsd)

### BAT
For beutifying the CAT command output download [here](https://github.com/sharkdp/bat)

### Background

[<img src="./assets/bg.jpg" alt="drawing" width="400"/>](./assets/bg.jpg)

### Font
For the Terminal and VSCode font I've chosen [JetBrains Mono](https://www.jetbrains.com/lp/mono/). Just remember download and install the one without "NL" on it as the **Ligatures** are quite cool to look at when coding.

<img src="./assets/font.gif" alt="drawing" width="400"/>
<br /><br />


# MacOS Only
I tend to jump between OS's, but because of my job, and because it's such a great environment to work on when you are a dev, macOS is my default OS. So here are som things I need to setup whenever I get a new mac.

### Cool commands to add
[**Youtube**](https://www.youtube.com/watch?v=qOrlYzqXPa8&ab_channel=NetworkChuck)

### Terminal theme
[**Adventure Time**](./assets/AdventureTime.terminal) is a cool terminal theme called **Adventure Time**

### External Monitor Brigthness
[**Better Display**](https://github.com/waydabber/BetterDisplay) is a cool open source app that allows you to control the brightness of any external monitor (Apple or not) using just the regular keys for that. **This changes the ACTUAL brightness so it's not just a mask applied to that screen**

### Mouse Acceleration
Some people find really annoying using the Magic Mouse since it's acceleration is a bit off, well I'm one of those people and I found [**Linear Mouse**](https://github.com/linearmouse/linearmouse) that let's you tweak your mouse like never before.

### Window Management App
So for some of us that used **windows** heavily before coming to the Mac, [**Rectangle**](https://github.com/rxhanson/Rectangle) is like a dream came true.

### Brew updated
For this you need to have brew installed. When you want to install something with brew it usually needs to update other packages first, so, let's try to keep brew updated...
```
0 20 * * * /opt/homebrew/bin/brew update
0 21 * * * /opt/homebrew/bin/brew upgrade
```

### Scripts
I have a couple of scripts that I use to automate some stuff, like:
- [**Check SSO**](./scripts/check-sso.sh): This script checks if the SSO is still valid and if not, it will ask you to login again
- [**Check PR**](./scripts/check-pr.sh): This script checks if there are any PR's assigned to you that are still open
- [**Assume Role**](./scripts/assume-role.sh): This script lets you assume a role and set the AWS_ACCESS_KEY and AWS_SECRET_ACCESS_KEY env vars automatically
- [**Retrier**](./scripts/retrier.sh): This script will retry a command until it succeeds or until the max retries are reached.It's really usefull when you are running a command that sometimes fails and you want to keep trying until it succeeds. ([Source](https://gist.github.com/jdormit/a4f963f0b6ec92011d04b8d26930198a))

# Linux Only
### Emojis
Sometimes in Arch, emojis do not work as expected. In order to setup emoji fonts, execute [this](./assets/emoji.sh) script
