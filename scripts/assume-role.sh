#!/bin/bash

role_name="$1"
forced_account_id="$2"
duration_seconds="${3:-3600}"
account_id="${forced_account_id:-$(aws sts get-caller-identity --query 'Account' --output text)}"
role_arn="arn:aws:iam::${account_id}:role/${role_name}"
session_name="AWS_CLI_DVR_$(date +'%Y-%m-%dT%H-%M-%S')"

creds="$(aws sts assume-role --role-arn ${role_arn} --role-session-name ${session_name} --duration-seconds ${duration_seconds} --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken,Expiration]' --output text)"

export AWS_ACCESS_KEY_ID="$(echo "$creds" | awk '{print $1}')"
export AWS_SECRET_ACCESS_KEY="$(echo "$creds" | awk '{print $2}')"
export AWS_SESSION_TOKEN="$(echo "$creds" | awk '{print $3}')"
EXPIRATION="$(echo "$creds" | awk '{print $4}')"

# Extracting date and time
EXPIRATION_DATE="${EXPIRATION:0:10}"
EXPIRATION_TIME="${EXPIRATION:11:8}"

# Checking if the expiration date is today or tomorrow
TODAY=$(date '+%Y-%m-%d')
TOMORROW=$(date -v+1d '+%Y-%m-%d')

if [[ "$EXPIRATION_DATE" == "$TODAY" ]]; then
  DATE_STRING="Today"
elif [[ "$EXPIRATION_DATE" == "$TOMORROW" ]]; then
  DATE_STRING="Tomorrow"
else
  DATE_STRING="$EXPIRATION_DATE"
fi

# Find the current timezone offset in seconds
OFFSET_SECONDS=$(date +%z | awk '{print substr($1,1,3)*3600 + substr($1,4,2)*60}')

# Convert the given time to seconds from the start of the day
EXPIRATION_SECONDS=$((10#${EXPIRATION_TIME:0:2} * 3600 + 10#${EXPIRATION_TIME:3:2} * 60 + 10#${EXPIRATION_TIME:6:2}))

# Add the offset
LOCAL_SECONDS=$((EXPIRATION_SECONDS + OFFSET_SECONDS))

# Convert seconds back to time format
LOCAL_HOUR=$((LOCAL_SECONDS / 3600))
LOCAL_MINUTE=$(((LOCAL_SECONDS % 3600) / 60))
LOCAL_SECOND=$((LOCAL_SECONDS % 60))

LOCAL_TIME=$(printf "%02d:%02d:%02d" $LOCAL_HOUR $LOCAL_MINUTE $LOCAL_SECOND)

# Extract current offset in hours for printing
OFFSET=$(date +%z | sed 's/00$//')

# Output the formatted string
EXPIRATION="$DATE_STRING at $LOCAL_TIME (UTC$OFFSET)"

echo "Assumed role $role_arn for $duration_seconds seconds. Expires at $EXPIRATION"
