#! /bin/bash

get_arn_last_segment() {

  read ARN

  EMAIL=${ARN##*/}

  echo $EMAIL

}

aws codecommit list-repositories --query 'repositories[*].repositoryName' --output json | jq -r '.[]' | while read repo; do
  echo "Checking $repo"
  echo ""
  # list all PRs for the repo
  aws codecommit list-pull-requests --repository-name $repo --pull-request-status OPEN --query 'pullRequestIds' --output json | jq -r '.[]' | while read pr; do

    # get the PR details
    pr_details=$(aws codecommit get-pull-request --pull-request-id $pr --output json)

    pr_author=$(echo "$pr_details" | jq -r '.pullRequest.authorArn' | get_arn_last_segment)

    pr_revision_id=$(echo "$pr_details" | jq -r '.pullRequest.revisionId')
    pr_title=$(echo "$pr_details" | jq -r '.pullRequest.title')
    pr_status=$(echo "$pr_details" | jq -r '.pullRequest.pullRequestStatus')
    approval_states=$(aws codecommit get-pull-request-approval-states --pull-request-id $pr --revision-id $pr_revision_id --output json)
    num_approvals=$(echo $approval_states | jq '.approvals | length')
    approvers=()
    for ((i = 0; i < $num_approvals; i++)); do
      approval=$(echo $approval_states | jq ".approvals[$i].approvalState")
      if [ "$approval" == "\"APPROVE\"" ]; then
        approverArn=$(echo $approval_states | jq -r ".approvals[$i].userArn" | get_arn_last_segment)
        approvers+=("$approverArn")
      fi
    done

    echo "============== PR $pr: $pr_title ============="
    echo ""
    echo "  Author: $pr_author"
    if [[ " ${approvers[@]} " =~ " d.velarde@tm-pro.eu " ]]; then
      echo "  Status: Approved ✅"
    else
      echo "  Status: $pr_status"
    fi

    echo "  Approvals:"
    for approver in "${approvers[@]}"; do
      echo "    - $approver"
    done
    echo "  URL: https://eu-central-1.console.aws.amazon.com/codesuite/codecommit/repositories/$repo/pull-requests/$pr/changes?region=eu-central-1"
    echo ""

  done
done
