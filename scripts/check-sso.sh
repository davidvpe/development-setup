#! /bin/bash

get_expiration_time() {

  start_url=$1

  expiration_time=$(grep -h $start_url ~/.aws/sso/cache/*.json 2>/dev/null)

  expiration_time=$(echo $expiration_time | jq .expiresAt | xargs -I {} sh -c 'TZ="UTC" date -j -f "%Y-%m-%dT%H:%M:%S" "+%s" $1 2>/dev/null' -- {})

  echo $expiration_time
}

current_time=$(date +%s)
start_url=$(aws configure get sso_start_url --profile ${AWS_PROFILE:=default})

if [ "$1" = "-f" ]; then
  aws sso login --profile ${AWS_PROFILE:=default}
  expiration_time=$(get_expiration_time $start_url)
else
  # find file that has the start url or call aws sso login
  if [ -z "$(grep -l $start_url ~/.aws/sso/cache/*.json 2>/dev/null)" ]; then
    # aws sso login --profile ${AWS_PROFILE:=default}
    aws sso login --profile ${AWS_PROFILE:=default}
  fi

  expiration_time=$(get_expiration_time $start_url)

  # if expiration time is less than 5 minutes greater than current time, call aws sso login
  if [ $((expiration_time - current_time)) -lt 300 ]; then
    aws sso login --profile ${AWS_PROFILE:=default}
    expiration_time=$(get_expiration_time $start_url)
  fi

fi

echo "Session expires at $(date -r $expiration_time)"
